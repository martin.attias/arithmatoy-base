#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Include utils.h
#include "utils.h"

int VERBOSE_MODE = 0;

// Function to get all digits and letters in base 36
const char *get_all_digits() { return "0123456789abcdefghijklmnopqrstuvwxyz"; }
const size_t DIGIT_COUNT = 36;

// Free memory allocated for a number
void free_arithmatoy(char *number) { free(number); }

// Function to add two numbers in any base
char *add_arithmatoy(unsigned int base, const char *lhs, const char *rhs) {
    if (VERBOSE_MODE) {
        fprintf(stderr, "add %u %s %s --verbose\n", base, lhs, rhs);
    }

    char *lhs_log = strdup(lhs);
    char *rhs_log = strdup(rhs);

    if (lhs == NULL && rhs == NULL) {
        return NULL;
    }

    if (rhs == NULL) {
        return strdup(lhs);
    }

    if (lhs == NULL) {
        return strdup(rhs);
    }

    lhs = remove_leading_zeros(lhs);
    rhs = remove_leading_zeros(rhs);

    lhs = reverse_string(strdup(lhs));
    rhs = reverse_string(strdup(rhs));

    const size_t lhs_len = strlen(lhs);
    const size_t rhs_len = strlen(rhs);
    const size_t max_len = lhs_len > rhs_len ? lhs_len : rhs_len;
    const size_t buffer_len = max_len + 2; // +1 for carry, +1 for \0
    char *buffer = (char *)malloc(buffer_len * sizeof(char));

    unsigned int carry = 0;
    unsigned int i = 0;

    while (i < max_len) {
        unsigned int lhs_digit = i < lhs_len ? get_digit_value(lhs[i]) : 0;
        unsigned int rhs_digit = i < rhs_len ? get_digit_value(rhs[i]) : 0;
        unsigned int sum = lhs_digit + rhs_digit + carry;
        carry = sum / base;
        buffer[i] = to_digit(sum % base);
        ++i;
    }

    if (carry > 0) {
        buffer[i] = to_digit(carry);
        ++i;
    }

    buffer[i] = '\0';
    buffer = reverse_string(buffer);
    buffer = (char *)remove_leading_zeros(buffer);
    if (VERBOSE_MODE) {
        fprintf(stderr, "%s + %s = %s\n", lhs_log, rhs_log, buffer);
        fprintf(stderr, "add: entering function\n");
    }
    return buffer;
}

// Function to subtract two numbers in any base
char *subtract_arithmatoy(unsigned int base, const char *lhs, const char *rhs) {
    if (VERBOSE_MODE) {
        fprintf(stderr, "subtract %u %s %s --verbose\n", base, lhs, rhs);
    }

    char *lhs_log = strdup(lhs);
    char *rhs_log = strdup(rhs);

    if (lhs == NULL && rhs == NULL) {
        return NULL;
    }

    if (rhs == NULL) {
        return strdup(lhs);
    }

    if (lhs == NULL) {
        return strdup(rhs);
    }

    lhs = remove_leading_zeros(lhs);
    rhs = remove_leading_zeros(rhs);

    if (strlen(lhs) < strlen(rhs) || (strlen(lhs) == strlen(rhs) && get_digit_value(lhs[0]) < get_digit_value(rhs[0]))) {
        return NULL;
    }

    lhs = reverse_string(strdup(lhs));
    rhs = reverse_string(strdup(rhs));

    const size_t lhs_len = strlen(lhs);
    const size_t rhs_len = strlen(rhs);
    const size_t max_len = lhs_len > rhs_len ? lhs_len : rhs_len;
    const size_t buffer_len = max_len + 1; // +1 for \0 (no carry)
    char *buffer = (char *)malloc(buffer_len * sizeof(char));

    unsigned int borrow = 0;
    unsigned int i = 0;

    while (i < max_len) {
        unsigned int lhs_digit = i < lhs_len ? get_digit_value(lhs[i]) : 0;
        unsigned int rhs_digit = i < rhs_len ? get_digit_value(rhs[i]) : 0;
        int diff = lhs_digit - rhs_digit - borrow;
        if (diff < 0) {
            borrow = 1;
            diff += base;
        } else {
            borrow = 0;
        }
        buffer[i] = to_digit(diff);
        ++i;
    }

    buffer[i] = '\0';
    buffer = reverse_string(buffer);
    buffer = (char *)remove_leading_zeros(buffer);
    if (VERBOSE_MODE) {
        fprintf(stderr, "%s - %s = %s\n", lhs_log, rhs_log, buffer);
        fprintf(stderr, "subtract: entering function\n");
    }
    return buffer;
}

// Function to multiply two numbers in any base
char *multiply_arithmatoy(unsigned int base, const char *lhs, const char *rhs) {
    if (VERBOSE_MODE) {
        fprintf(stderr, "multiply %u %s %s --verbose\n", base, lhs, rhs);
    }

    char *lhs_log = strdup(lhs);
    char *rhs_log = strdup(rhs);

    if (lhs == NULL && rhs == NULL) {
        return NULL;
    }

    if (rhs == NULL) {
        return strdup(lhs);
    }

    if (lhs == NULL) {
        return strdup(rhs);
    }

    lhs = remove_leading_zeros(lhs);
    rhs = remove_leading_zeros(rhs);

    lhs = reverse_string(strdup(lhs));
    rhs = reverse_string(strdup(rhs));

    const size_t lhs_len = strlen(lhs);
    const size_t rhs_len = strlen(rhs);
    const size_t buffer_len = lhs_len + rhs_len + 1; // +1 for \0
    char *buffer = (char *)malloc(buffer_len * sizeof(char));

    for (size_t i = 0; i < buffer_len; ++i) {
        buffer[i] = '0';
    }

    for (size_t i = 0; i < lhs_len; ++i) {
        unsigned int lhs_digit = get_digit_value(lhs[i]);
        unsigned int carry = 0;
        for (size_t j = 0; j < rhs_len; ++j) {
            unsigned int rhs_digit = get_digit_value(rhs[j]);
            unsigned int product = lhs_digit * rhs_digit + carry + get_digit_value(buffer[i + j]);
            carry = product / base;
            buffer[i + j] = to_digit(product % base);
        }
        buffer[i + rhs_len] = to_digit(carry);
    }

    buffer[buffer_len - 1] = '\0';
    buffer = reverse_string(buffer);
    buffer = (char *)remove_leading_zeros(buffer);
    if (VERBOSE_MODE) {
        fprintf(stderr, "%s * %s = %s\n", lhs_log, rhs_log, buffer);
        fprintf(stderr, "multiply: entering function\n");
    }
    return buffer;
}

// Utility functions

unsigned int get_digit_value(char digit) {
    // Convert a digit to its integer value
    if (digit >= '0' && digit <= '9') {
        return digit - '0';
    }
    if (digit >= 'a' && digit <= 'z') {
        return 10 + (digit - 'a');
    }
    return -1;
}

char to_digit(unsigned int value) {
    // Convert an integer value to a digit
    if (value >= DIGIT_COUNT) {
        debug_abort("Invalid value for to_digit()");
        return 0;
    }
    return get_all_digits()[value];
}

char *reverse_string(char *str) {
    // Reverse a string in place
    const size_t length = strlen(str);
    const size_t bound = length / 2;
    for (size_t i = 0; i < bound; ++i) {
        char tmp = str[i];
        const size_t mirror = length - i - 1;
        str[i] = str[mirror];
        str[mirror] = tmp;
    }
    return str;
}

const char *remove_leading_zeros(const char *number) {
    // Remove leading zeros from a number
    if (*number == '\0') {
        return number;
    }
    while (*number == '0') {
        ++number;
    }
    if (*number == '\0') {
        --number;
    }
    return number;
}

void debug_abort(const char *debug_msg) {
    // Print a message and exit
    fprintf(stderr, "%s", debug_msg);
    exit(EXIT_FAILURE);
}
